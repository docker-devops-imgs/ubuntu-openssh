FROM ubuntu:22.10

# Installing openssh-client
RUN apt-get update && apt-get -y upgrade && apt-get install openssh-client -y

# Define working directory.
WORKDIR /home/ubuntu

# Define default command.
CMD ["bash"]